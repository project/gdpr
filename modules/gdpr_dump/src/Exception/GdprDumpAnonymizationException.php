<?php

namespace Drupal\gdpr_dump\Exception;

/**
 * The GDPR Dump Anonymization Exception.
 *
 * @package Drupal\gdpr_dump\Exception
 */
class GdprDumpAnonymizationException extends \Exception {

}
