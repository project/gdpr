<?php

namespace Drupal\gdpr_dump\Sql;

use Drush\Sql\SqlSqlite;

/**
 * The GDPR GdprSqlSqlite.
 *
 * @package Drupal\gdpr_dump\Sql
 */
class GdprSqlSqlite extends SqlSqlite {
}
