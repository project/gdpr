<?php

namespace Drupal\Tests\gdpr_fields\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests GDPR Fields and settings.
 *
 * @group gdpr
 */
class GdprFieldsTest extends BrowserTestBase {
  use NodeCreationTrait;
  use ContentTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'gdpr',
    'gdpr_fields',
    'gdpr_tasks',
    'gdpr_consent',
    'anonymizer',
    'node',
    'field_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create the users used for the tests.
    $this->drupalLogin($this->rootUser);
  }

  /**
   * {@inheritdoc}
   */
  public function testGdprFieldsSettings() {

    // Create a content type.
    $this->createContentType(['type' => 'page']);

    // Add field to content type and enable GDPR settings.
    FieldStorageConfig::create([
      'field_name' => 'field_gdpr',
      'entity_type' => 'node',
      'type' => 'string',
      'cardinality' => 1,
      'locked' => FALSE,
      'indexes' => [],
      'settings' => [
        'max_length' => 255,
        'case_sensitive' => FALSE,
        'is_ascii' => FALSE,
      ],
    ])->save();

    FieldConfig::create([
      'field_name' => 'field_gdpr',
      'entity_type' => 'node',
      'label' => 'GDPR Settings',
      'bundle' => 'page',
      'description' => '',
      'required' => FALSE,
      'settings' => [
        'gdpr_enabled' => TRUE,
      ],
    ])->save();

    // Enable GDPR settings.
    $this->drupalGet('/admin/structure/types/manage/page/fields/node.page.field_gdpr');
    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $assertSession */
    $assertSession = $this->assertSession();
    $assertSession->statusCodeEquals(200);
    $assertSession->fieldExists('This is a GDPR field')->check();
    $this->getSession()->getPage()->pressButton('Save');

    // Verify field is GDPR enabled.
    $this->assertTrue($this->config('gdpr_fields.gdpr_fields_config.node')->get('bundles.page.field_gdpr.enabled'), 'The GDPR field has GDPR enabled.');

  }

}
