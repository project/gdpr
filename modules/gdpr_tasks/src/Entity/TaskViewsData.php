<?php

namespace Drupal\gdpr_tasks\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Task entities.
 */
class TaskViewsData extends EntityViewsData {

}
